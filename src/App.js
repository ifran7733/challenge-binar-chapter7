import 'bootstrap/dist/css/bootstrap.min.css';
import { Routes, Route } from 'react-router-dom';
import CariMobil from './components/carimobil';
import LandingPage from './components/landingPage';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<LandingPage />} />
        <Route path="/cari-mobil" element={<CariMobil />} />
      </Routes>
    </div>
  );
}

export default App;
