import './CarouselItem.css';
import { Carousel } from 'react-bootstrap';
const CarouselItem = () => {
  return (
    <div class="container">
      <div class="title-carousel text-center">
        <h2 class="fw-bold mb-2">Testimonial</h2>
        <p class="mb-4">Mengapa harus pilih Binar Car Rental?</p>
      </div>
      <Carousel>
        <Carousel.Item>
          <div class="item">
            <div class="card-testimonial pt-4 pb-4">
              <div class="row">
                <div class="col-1"></div>
                <div class="col-2">
                  <img
                    src="images/pp1.png"
                    class="pt-5 img-fluid rounded-start"
                  />
                </div>
                <div class="col-md-9">
                  <div class="card-body">
                    <img src="icon/Rate.png" alt="" />
                    <p class="card-text">
                      "Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                      Numquam, praesentium error fugit exercitationem pariatur
                      beatae veritatis consectetur cumque quam?"
                    </p>
                    <p class="card-text fw-bold">Alice 32, Bromo</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Carousel.Item>
        <Carousel.Item>
          <div class="item">
            <div class="card-testimonial pt-4 pb-4">
              <div class="row">
                <div class="col-1"></div>
                <div class="col-2">
                  <img
                    src="images/pp4.png"
                    class="pt-5 img-fluid rounded-start"
                  />
                </div>
                <div class="col-md-9">
                  <div class="card-body">
                    <img src="icon/Rate.png" alt="" />
                    <p class="card-text">
                      "Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                      Numquam, praesentium error fugit exercitationem pariatur
                      beatae veritatis consectetur cumque quam?"
                    </p>
                    <p class="card-text fw-bold">John Dee 32, Bromo</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Carousel.Item>
        <Carousel.Item>
          <div class="item">
            <div class="card-testimonial pt-4 pb-4">
              <div class="row">
                <div class="col-1"></div>
                <div class="col-2">
                  <img
                    src="images/pp3.png"
                    class="pt-5 img-fluid rounded-start"
                  />
                </div>
                <div class="col-md-9">
                  <div class="card-body">
                    <img src="icon/Rate.png" alt="" />
                    <p class="card-text">
                      "Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                      Numquam, praesentium error fugit exercitationem pariatur
                      beatae veritatis consectetur cumque quam?"
                    </p>
                    <p class="card-text fw-bold">John Dee 32, Bromo</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Carousel.Item>
      </Carousel>
    </div>
  );
};

export default CarouselItem;
