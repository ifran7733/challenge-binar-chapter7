import './Footer.css';
import { Nav } from 'react-bootstrap';
const Footer = () => {
  return (
    <div className="container p-4 text-black">
      <div className="row">
        <div className="col-lg-4 col-md-12 mb-4 mb-md-0">
          <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
          <p>binarcarrental@gmail.com</p>
          <p>081-233-334-808</p>
        </div>
        <div className="col-lg-2 col-md-6 mb-4 mb-md-0 fw-bold">
          <p>Our Services</p>
          <p>Why Us</p>
          <p>Testimonial</p>
          <p>FAQ</p>
        </div>
        <div className="rate col-lg-3 col-md-6 mb-4 mb-md-0">
          <p className="text-black">Connect with Us</p>
          <span>
            <a href="#">
              <img src="icon/icon_facebook.png" alt="fb" />
            </a>
          </span>
          <span>
            <a href="#">
              <img src="icon/icon_instagram.png" alt="ig" />
            </a>
          </span>
          <span>
            <a href="#">
              <img src="icon/icon_twitter.png" alt="tw" />
            </a>
          </span>
          <span>
            <a href="#">
              <img src="icon/icon_mail.png" alt="mail" />
            </a>
          </span>
          <span>
            <a href="#">
              <img src="icon/icon_twitch.png" alt="twich" />
            </a>
          </span>
        </div>
        <div className="col-lg-2 col-md-6 mb-4 mb-md-0">
          <Nav defaultActiveKey="/home" as="ul" className="list-unstyled">
            <Nav.Item as="li">
              <p className="text-black">Copyright Binar 2022</p>
            </Nav.Item>
            <Nav.Item as="li" className="square-footer">
              <Nav.Link
                eventKey="link-1"
                className="text-black square-footer"
              ></Nav.Link>
            </Nav.Item>
          </Nav>
        </div>
      </div>
    </div>
  );
};

export default Footer;
