import { Link } from 'react-router-dom';
import './Hero.css';
const Hero = () => {
  return (
    <div className="jumbotron position-relative">
      <div className="container">
        <div className="desc-hero w-desc">
          <p className="fw-bold pe-5 title-jumb">
            Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)
          </p>
          <p className="pe-5 desc-jumb">
            Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas
            terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu
            untuk sewa mobil selama 24 jam.
          </p>
          <Link to="/cari-mobil" className="btn btn-g-green">
            Mulai Sewa Mobil
          </Link>
        </div>
      </div>
      <div className="container-car position-absolute bottom-0 end-0">
        <div className="square-car"></div>
        <img
          className="car position-relative bottom-0 end-0"
          src="images/car.png"
          alt="car"
          width="100%"
        />
      </div>
    </div>
  );
};

export default Hero;
