import { Navbar, Container, Nav } from 'react-bootstrap';
import './NavigasiMenu.css';
function NavigasiMenu() {
  return (
    <div>
      <Navbar expand="lg" className="nav" fixed="top">
        <Container>
          <Navbar.Brand href="#home" className="navbar-brand"></Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto ">
              <Nav.Link href="#home">Our Services</Nav.Link>
              <Nav.Link href="#link1">Why Us</Nav.Link>
              <Nav.Link href="#link2">Testimonial</Nav.Link>
              <Nav.Link href="#link3">FAQ</Nav.Link>
              <Nav.Link href="#link4" className="btn btn-g-green">
                Registrasi
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
}
export default NavigasiMenu;
