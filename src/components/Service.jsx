import './Service.css';
const Service = () => {
  return (
    <div className="our-service overflow-auto">
      <div className="container position-relative">
        <div className="img-user w-50 position-relative float-start">
          <img
            src="./images/girl.png"
            alt="user"
            className="img-fluid position-absolute top-50 start-50 translate-middle"
          />
          <div className="circle-green rounded-circle position-absolute top-0 start-0"></div>
          <div className="circle-yellow rounded-circle bg-warning position-absolute bottom-0 start-0 m-4"></div>
          <div className="circle-red rounded-circle position-absolute top-0 end-0 m-5"></div>
        </div>

        <div className="list-service w-50 float-end">
          <h3 className="title-service fw-bold">
            Best Car Rental for any kind of trip in (Lokasimu)!
          </h3>
          <br />
          <p className="desc-service text-wrap overflow-auto">
            Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga
            lebih murah dibandingkan yang lain, kondisi mobil baru, serta
            kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding,
            meeting, dll.
          </p>
          <p>
            <img src="icon/cek.png" alt="cek" />
            <span className="list-desc">
              Sewa Mobil Dengan Supir di Bali 12 Jam
            </span>
          </p>
          <p>
            <img src="icon/cek.png" alt="cek" />
            <span className="list-desc">
              Sewa Mobil Lepas Kunci di Bali 24 Jam
            </span>
          </p>
          <p>
            <img src="icon/cek.png" alt="cek" />
            <span className="list-desc">
              {' '}
              Sewa Mobil Jangka Panjang Bulanan{' '}
            </span>
          </p>
          <p>
            <img src="icon/cek.png" alt="cek" />
            <span className="list-desc">
              Gratis Antar - Jemput Mobil di Bandara
            </span>
          </p>
          <p>
            <img src="icon/cek.png" alt="" />
            <span className="list-desc">
              Layanan Airport Transfer / Drop In Out
            </span>
          </p>
        </div>
      </div>
    </div>
  );
};
export default Service;
