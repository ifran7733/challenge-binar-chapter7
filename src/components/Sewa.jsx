import './Sewa.css';
import { Button } from 'react-bootstrap';
const Sewa = () => {
  return (
    <div>
      <section className="desc-mobil">
        <div className="container text-center rounded-3">
          <h2 className="title-desc-car fw-bold">
            Sewa Mobil di (Lokasimu) Sekarang
          </h2>
          <p className="lh-sm">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt.
          </p>
          <p className="lh-sm">
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
          </p>
          <Button variant="primary" className="btn btn-green">
            Mulai Sewa Mobil
          </Button>{' '}
        </div>
      </section>
    </div>
  );
};

export default Sewa;
