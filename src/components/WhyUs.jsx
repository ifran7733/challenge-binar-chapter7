import './WhyUs.css';
const WhyUs = () => {
  return (
    <div className="container why-us">
      <h2 className="fw-bold mb-4">Why Us?</h2>
      <p className="mb-4">Mengapa harus pilih Binar Car Rental?</p>
      <div className="row row-cols-1 row-cols-md-4 g-4">
        <div className="col">
          <div className="card h-100">
            <div className="card-body">
              <img src="icon/icon_complete.png" alt="" className="mb-4" />
              <h5 className="card-title fw-bold">Mobil Lengkap</h5>
              <p className="card-text">
                Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan
                terawat
              </p>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card h-100">
            <div className="card-body">
              <img
                src="icon/icon_price.png"
                alt="icon-price"
                className="mb-4"
              />
              <h5 className="card-title fw-bold">Harga Murah</h5>
              <p className="card-text">
                Harga murah dan bersaing, bisa bandingkan harga kami dengan
                rental mobil lain
              </p>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card h-100">
            <div className="card-body">
              <img src="icon/icon_24hrs.png" alt="icon-24" className="mb-4" />
              <h5 className="card-title fw-bold">Layanan 24 Jam</h5>
              <p className="card-text">
                Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga
                tersedia di akhir minggu
              </p>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card h-100">
            <div className="card-body">
              <img
                src="icon/icon_professional.png"
                alt="icon-pro"
                className="mb-4"
              />
              <h5 className="card-title fw-bold">Sopir Profesional</h5>
              <p className="card-text">
                Sopir yang profesional, berpengalaman, jujur, ramah dan selalu
                tepat waktu
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default WhyUs;
