import './FormCari.css';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { filterCars } from '../../store/actions';

const FormCari = () => {
  const dispatch = useDispatch();

  const [pickUpTime, setPickUpTime] = useState('');
  const [date, setDate] = useState('');
  const [capacity, setCapacity] = useState('');

  const handleDate = (e) => {
    setDate(e.target.value);
  };

  const handleTimeChange = (e) => {
    setPickUpTime(e.target.value);
  };

  const handleCapacity = (e) => {
    setCapacity(e.target.value);
  };

  const handleFilterCar = () => {
    let timeValue = pickUpTime;
    let dateValue = date;
    let passangerValue = Number(capacity);
    let dateTimeValue = Date.parse(dateValue + 'T' + timeValue);
    // let splitTime = pickUpTime.split(':');
    // let timeInSecond = +splitTime[0] * 60 * 60 + +splitTime[1] * 60;

    dispatch(
      filterCars((car) => {
        let result = true;

        // let available = car.availableAt.split('T');
        // let availableDate = Date.parse(available[0]);
        // let availableTime = available[1].split(':');
        // let availableTimeInSecond =
        //   +availableTime[0] * 60 * 60 +
        //   (+availableTime[1] * 60 + +availableTime[2].split('.')[0]);

        // if (dateTimeValue && passangerValue) {
        //   result =
        //     Date.parse(car.availableAt) >= dateTimeValue &&
        //     car.capacity >= passangerValue;
        // } else
        if (dateTimeValue) {
          result = Date.parse(car.availableAt) >= dateTimeValue;
        } else if (passangerValue) {
          result = car.capacity >= passangerValue;
        }
        // else if (dateValue) {
        //   result = Date.parse(availableDate) >= dateTimeValue;
        // }
        // else if (timeValue) {
        //   result = availableTimeInSecond >= timeInSecond;
        // }
        // console.log(result);
        return result;
      })
    );
  };

  return (
    <div className="container rounded">
      <div className="carimobil-form row g-3 d-flex justify-content-center p-3">
        <div class="col-2">
          <label for="exampleFormControlInput1" class="form-label">
            Tipe Driver
          </label>
          <select
            class="form-select"
            aria-label="Default select example"
            id="selectDrive"
          >
            <option selected>Pilih Tipe Driver</option>
            <option value="1">Dengan Sopir</option>
            <option value="2">Tanpa Sopir</option>
          </select>
        </div>
        <div className="col-auto">
          <label className="form-label">Tanggal</label>
          <input
            type="date"
            className="form-control"
            id="tanggal"
            // onChange={(e) => handleDate(e)}
          />
        </div>

        <div class="col-3">
          <label for="exampleFormControlInput1" class="form-label">
            Waktu Jemput
          </label>
          <select
            class="form-select"
            aria-label="Default select example"
            id="selectWaktu"
            onChange={(e) => handleTimeChange(e)}
          >
            <option selected>Pilih Waktu</option>
            <option value="00:00">00:00 WIB</option>
            <option value="01:00">01:00 WIB</option>
            <option value="02:00">02:00 WIB</option>
            <option value="03:00">03:00 WIB</option>
            <option value="04:00">04:00 WIB</option>
            <option value="05:00">05:00 WIB</option>
            <option value="06:00">06:00 WIB</option>
            <option value="07:00">07:00 WIB</option>
            <option value="08:00">08:00 WIB</option>
            <option value="09:00">09:00 WIB</option>
            <option value="10:00">10:00 WIB</option>
            <option value="11:00">11:00 WIB</option>
            <option value="12:00">12:00 WIB</option>
          </select>
        </div>
        <div className="col-auto">
          <label className="form-label">Jumlah Orang</label>
          <input
            type="number"
            className="form-control"
            id="orang"
            onChange={(e) => handleCapacity(e)}
          />
        </div>
        <div className="col-auto pt-4">
          <button
            className="btn btn-g-green mb-3"
            id="btn-car"
            onClick={() => handleFilterCar()}
          >
            Cari Mobil
          </button>
        </div>
      </div>
      <div
        id="cars-container"
        className="p-4 d-flex flex-wrap justify-content-center"
      ></div>
    </div>
  );
};
export default FormCari;
