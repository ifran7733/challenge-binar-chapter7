import axios from 'axios';
import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import './ResultCari.css';
const ResultCari = () => {
  const [cars, setCar] = useState([]);
  const items = useSelector((state) => state.car.cars);

  const getCarsData = async () => {
    try {
      const response = await axios.get(
        'https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json'
      );
      setCar(response.data);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => getCarsData, []);
  return (
    <div className="container ">
      <div className="row justify-content-md-center">
        {items.map((car) => (
          <div className="card" key={car.id}>
            <img src={car.image} alt={car.name} className="card-img-top" />
            <div className="card-body">
              <p clas="item-name">
                <b>{car.type}</b>
              </p>
              <p className="plate">
                <b> Rp.{car.rentPerDay} / hari </b>
              </p>
              <p className="description">{car.description}</p>
              <div className="capacity-item">
                <p className="capacity">
                  <img src="icon/users.png" alt="cek" /> {car.capacity} orang
                </p>
              </div>
              <div className="transmission-item">
                <p className="tranmision">
                  <img src="icon/settings.png" alt="cek" /> {car.transmission}
                </p>
              </div>
              <div className="year-item">
                <p className="year">
                  <img src="icon/calendar.png" alt="cek" /> {car.year}
                </p>
              </div>
              <a href="#" className="btn btn-g-green">
                Sewa
              </a>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
export default ResultCari;
