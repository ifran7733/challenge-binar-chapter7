import NavigasiMenu from '../NavigasiMenu';
import Hero from '../Hero';
import Footer from '../Footer';
import FormCari from './FormCari';
import ResultCari from './ResultCari';

const CariMobil = () => {
  return (
    <div>
      <NavigasiMenu />
      <Hero />
      <FormCari />
      <ResultCari />
      <Footer />
    </div>
  );
};

export default CariMobil;
