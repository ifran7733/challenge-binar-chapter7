import NavigasiMenu from '../NavigasiMenu';
import Hero from '../Hero';
import Service from '../Service';
import WhyUs from '../WhyUs';
import Sewa from '../Sewa';
import Faq from '../Faq';
import Footer from '../Footer';
import CarouselItem from '../CarouselItem';

const LandingPage = () => {
  return (
    <div>
      <NavigasiMenu />
      <Hero />
      <Service />
      <WhyUs />
      <CarouselItem />
      <Sewa />
      <Faq />
      <Footer />
    </div>
  );
};

export default LandingPage;
