import axios from 'axios';

export const FILTER_CARS = 'FILTER_CARS';
export const filterCars = (filterer) => async (dispatch) => {
  try {
    if (filterer instanceof Function) {
      const json = await axios.get(
        'https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json'
      );
      dispatch({
        type: FILTER_CARS,
        payload: json.data.filter(filterer),
      });
    }
  } catch (error) {
    console.log(error);
  }
};
