import { FILTER_CARS } from './../actions';

const initialState = {
  cars: [],
};

const carsReducers = (state = initialState, action) => {
  switch (action.type) {
    case FILTER_CARS:
      return { ...state, cars: action.payload };
    default:
      return state;
  }
};

export default carsReducers;
