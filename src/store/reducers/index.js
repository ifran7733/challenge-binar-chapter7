// Import module combine
import { combineReducers } from 'redux';

// import reducers
import carsReducers from './carReducer';

const reducers = combineReducers({
  car: carsReducers,
});

export default reducers;
